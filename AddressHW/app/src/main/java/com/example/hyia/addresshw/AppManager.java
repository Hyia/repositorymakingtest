package com.example.hyia.addresshw;

/**
 * Created by hyia on 11/25/16.
 */

public class AppManager {
    private static AppManager instance;

    private MainActivity mainActivity;
    private DBHelper dbHelper;
    private boolean dbCreated = false;

    private AppManager(){
    }
    public static AppManager getInstance(){
        if(instance==null){
            instance=new AppManager();
        }
        return instance;
    }

    public void setMainActivity(MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }
    public void dBHelper_Init(){
            if(mainActivity!=null && dbCreated == false){
                dbHelper = new DBHelper(mainActivity);
            }
    }
    public MainActivity getMainActivity(){
        return mainActivity;
    }
    public DBHelper getDbHelper(){return dbHelper;}
}
