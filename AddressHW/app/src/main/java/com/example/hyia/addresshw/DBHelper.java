package com.example.hyia.addresshw;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = "DBHelper";
    private static final String DATABASE_NAME = "addresshw.db";
    public static final String ADDR_TABLE_NAME = "adhw";
    SQLiteDatabase mDB = null;

    private static final int DATABASE_VERSION = 3;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mDB = this.getWritableDatabase();
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS "+ADDR_TABLE_NAME+"(" +
                " name TEXT NOT NULL," +
                " tel TEXT NOT NULL PRIMARY KEY," +
                " regiday TEXT," +
//                " regiday DATETIME DEFUALT CURRENT_TIMESTAMP," +
                " locat TEXT,"+
                " picbitarry BYTE[]"+
                ");");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ADDR_TABLE_NAME);
        onCreate(db);
    }

    public Cursor select(String query){
        return mDB.rawQuery(query,null);
    }

    public SQLiteDatabase getmDB(){
        return mDB;
    }

    public void insert(String name, String tel, String regiday, String locat, byte[] pic){
        ContentValues content = new ContentValues();
        content.put("name",name);
        content.put("tel",tel);
        content.put("regiday",regiday);
        content.put("locat",locat);
        content.put("picbitarry",pic);

        mDB.insert(ADDR_TABLE_NAME,null,content);
    }

    public void update(String name, String tel, String regiday, String locat, byte[] pic){
        ContentValues content = new ContentValues();
        content.put("name",name);
        content.put("tel",tel);
        content.put("regiday",regiday);
        content.put("locat",locat);
        content.put("picbitarry",pic);

        mDB.update(ADDR_TABLE_NAME,content,"tel = ?",new String[]{tel});
    }

}

