package com.example.hyia.addresshw;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.sql.Time;
import java.util.Calendar;

/**
 * @author hyia
 *
 * Camera method
 * see https://developer.android.com/training/camera/photobasics.html
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private static final int ALIGN_BY_NAME =0;
    private static final int ALIGN_BY_DATE =1;

    private static final int MENU = 0;
    private static final int VIEW = 1;
    private static final int EDIT = 2;
    private static final int REGI = -1;

    private static final int REQUEST_IMG_CAPTURE = 1;

    private static int CURRENT_ALIGN_STATE=-1;
    private static int CURRENT_VIEW = 0;
    private boolean isAsc = true;

    private TableLayout mainTable;
    private TextView menu_text_name;
    private TextView menu_text_date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        AppManager.getInstance().setMainActivity(this);
        AppManager.getInstance().dBHelper_Init();

        mainTable = (TableLayout)findViewById(R.id.main_table);
        menu_text_name = (TextView)findViewById(R.id.dbkey_name);
        menu_text_date = (TextView)findViewById(R.id.dbkey_date);

    }

    private int ester = 0;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        Toast.makeText(this, "in onKeyDown && "+(keyCode==KeyEvent.KEYCODE_BACK), Toast.LENGTH_SHORT).show();
        if(keyCode == KeyEvent.KEYCODE_BACK){
            switch (CURRENT_VIEW){
                case MENU:
                    switch (ester){
                        case 0:
                            Toast.makeText(this, "한 번 더 뒤로가기를 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
                            ester++;
                            break;
                        case 1:
                            Toast.makeText(this, "거짓말이지롱", Toast.LENGTH_SHORT).show();
                            ester++;
                            break;
                        case 2:
                            Toast.makeText(this, "더 눌러봤자 꺼주진 않을거야", Toast.LENGTH_SHORT).show();
                            ester++;
                            break;
                        case 3:
                            Toast.makeText(this, "강종이나 하시지!", Toast.LENGTH_SHORT).show();
                            ester++;
                            break;
                        case 4:
                            Toast.makeText(this, "그렇게 자꾸 눌러도 뭐 나오지 않는데", Toast.LENGTH_SHORT).show();
                            ester++;
                            break;
                        case 5:
                            Toast.makeText(this, "아 고만해 귀찮아", Toast.LENGTH_SHORT).show();
                            ester++;
                            break;
                        case 6:
                            Toast.makeText(this, "어휴 징한놈", Toast.LENGTH_SHORT).show();
                            ester++;
                            break;
                        case 7:
                            ester = 0;
                            System.exit(0);
                            break;
                    }
                    break;
                case VIEW:
                case REGI:
                    goContentView(MENU);
                    break;
                case EDIT:
                    alignByname(null);
                    break;
            }
        }
        return false;
    }

    void goContentView(int where){
        switch (where){
            case MENU:
                this.setContentView(R.layout.activity_menu);
                CURRENT_VIEW = MENU;
                break;
            case VIEW:
                this.setContentView(R.layout.activity_main);
                CURRENT_VIEW = VIEW;
                break;
            case REGI:
                this.setContentView(R.layout.detail_layout);
                CURRENT_VIEW = REGI;
                break;
            case EDIT:
                this.setContentView(R.layout.detail_layout);
                CURRENT_VIEW = EDIT;
                break;
        }
    }

    void tableSprader(int mode){
        //TODO: 테이블을 가져와서. 정리 할 것.
        String sql_exec = new String();// making SQL string
        String table_label = new String();//append colum to notify asc or desc.

        menu_text_date = (TextView)findViewById(R.id.dbkey_date);
        menu_text_name = (TextView)findViewById(R.id.dbkey_name);
        TableLayout tableLayout = (TableLayout)findViewById(R.id.main_table);
        tableLayout.removeAllViews();

        //오름차, 내림차를 처리
        //같은 선택을 연속으로 하면 오름차, 내림차를 변경 가능하게 설정.
        if(mode != CURRENT_ALIGN_STATE || !isAsc){
            //asc
            table_label = "↑";
            isAsc = true;
        }else{
            //desc
            table_label = "↓";
            isAsc = false;
        }
        CURRENT_ALIGN_STATE = mode;

        DBHelper dbHelper = AppManager.getInstance().getDbHelper();
        final Cursor cursor;
        sql_exec = "SELECT name, tel, regiday FROM adhw ORDER BY";

        //어떤 속성을 기준으로 정리할 지 결정할 것.
        switch (mode){
            case ALIGN_BY_DATE:
                sql_exec = sql_exec + " regiday ";

                table_label ="날짜" + table_label;
                menu_text_date.setText(table_label);
                menu_text_name.setText("이름");
                break;
            case ALIGN_BY_NAME:
                sql_exec = sql_exec + " name ";

                table_label ="이름" + table_label;
                menu_text_date.setText("날짜");
                menu_text_name.setText(table_label);
                break;
        }
        if(isAsc){
            sql_exec = sql_exec + " asc;";
        }else{
            sql_exec = sql_exec + " desc;";
        }
        cursor =   dbHelper.select(sql_exec);

        for (int i = 0; cursor.moveToNext(); i++) {
            TableRow row = new TableRow(this);
            TableRow.LayoutParams rowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(rowParams);

            TextView nTextView = new TextView(this); TextView tTextView = new TextView(this);
            TextView rTextView = new TextView(this); TextView oTextView = new TextView(this);

            final String name = cursor.getString(0);
            final String tel = cursor.getString(1);
            final String regidate = cursor.getString(2);

            nTextView.setText(name); tTextView.setText(tel);
            rTextView.setText(regidate); oTextView.setText("...");
            oTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goContentView(EDIT);
                    Log.d(TAG,"tel is ["+tel+"]");
                    detailSprader(tel);
                }
            });
            row.addView(nTextView);
            row.addView(tTextView);
            row.addView(rTextView);
            row.addView(oTextView);

            tableLayout.addView(row,i);
        }


        cursor.close();
    }
    void detailSprader(String tel){
        String name = null, tele = null, regiday = null, locat = null;
        Bitmap pic;

        Log.d(TAG,"detailSparader in");

        EditText nameView = (EditText)findViewById(R.id.input_name);
        EditText telView = (EditText)findViewById(R.id.input_tel);
        TextView regidayView = (TextView)findViewById(R.id.input_date);
        EditText locatView = (EditText)findViewById(R.id.input_location);
        ImageView picView = (ImageView)findViewById(R.id.profile_picture);


        pic = ((BitmapDrawable)picView.getDrawable()).getBitmap();

        if(CURRENT_VIEW==REGI || tel==null){//new person register
            Log.d(TAG,"detailSparader: REGI");

            name="기본이름";
            tele = "000-000-0000";
            Calendar calendar = Calendar.getInstance();
//            regiday = calendar.getTime().toString();
            Time t = new Time(calendar.getTime().getTime());
            regiday =  calendar.get(Calendar.DATE)+"일 "+t.toString().substring(0,5);
            locat = "기본장소";

        }else if(CURRENT_VIEW==EDIT){//getting data from db by key (tel number)
            Log.d(TAG,"detailSparader: EDIT");
            Cursor cursor;
            DBHelper dbHelper = AppManager.getInstance().getDbHelper();
            String qur = "SELECT name, tel, regiday, locat, picbitarry from "+dbHelper.ADDR_TABLE_NAME+" where tel=\""+tel+"\"";
            cursor = dbHelper.select(qur);
            Log.d(TAG,"detailSparader: qurry["+qur+"]");
            Log.d(TAG,"detailSparader: cursor is null?["+(cursor==null?"null":"not null")+"]");
            for (int i = 0; cursor.moveToNext(); i++) {
                Log.d(TAG,"detailSparader: Cursor"+i);
                    name = cursor.getString(0);
                    tele = cursor.getString(1);
                    regiday = cursor.getString(2);
                    locat = cursor.getString(3);
//                switch (i){
//                    case 0: name = cursor.getString(0);break;
//                    case 1: tele = cursor.getString(1);break;
//                    case 2: regiday = cursor.getString(2);break;
//                    case 3: locat = cursor.getString(3);break;
//                }
                if(cursor.moveToFirst()){
                    byte[] imgByte = cursor.getBlob(4);
                    cursor.close();
                    Log.d(TAG,"detailSparader imgByteArray: "+imgByte.length);
                    pic = BitmapFactory.decodeByteArray(imgByte,0,imgByte.length);
                }else{
                    pic = ((BitmapDrawable)picView.getDrawable()).getBitmap();
                }
            }
            cursor.close();

            if(cursor!=null && !cursor.isClosed()){
                cursor.close();
            }
        }else{
            Log.d(TAG,"detailSparader: WTF ERROR");
            name="ERROR_IN_NAME";
            tele = "ERROR_IN_TEL";
            Calendar calendar = Calendar.getInstance();
            Time t = new Time(calendar.getTime().getTime());
            regiday =  calendar.get(Calendar.DATE)+"일 "+t.toString().substring(0,5);
            locat = "ERROR_IN_LOCATION";
            pic = ((BitmapDrawable)picView.getDrawable()).getBitmap();
        }

        nameView.setText(name);
        telView.setText(tele);
        regidayView.setText(regiday);
        locatView.setText(locat);

        picView.setImageBitmap(pic);
    }

    /**
     * make DB aline by people name
     * and also current defualt address book view.
     */
    void alignByname(View view){
//        Toast.makeText(this, "AlignByName", Toast.LENGTH_SHORT).show();
        if(view==null || view.getId()!=R.id.dbkey_name){
            goContentView(VIEW);
        }
        tableSprader(ALIGN_BY_NAME);
    }

    /**
     * make DB aline by registered date
     */
    void alignByDate(View view){
//        Toast.makeText(this, "AlignByDate", Toast.LENGTH_SHORT).show();
        if(view.getId()!=R.id.dbkey_date){
            goContentView(VIEW);
        }
        tableSprader(ALIGN_BY_DATE);
    }

    /**
     * go Register screen
     */
    void newbieComes(View view){
        goContentView(REGI);
        detailSprader(null);
    }

    /**
     * registration or modifing poeple
     */
    void saveModi(View view){
        Log.d(TAG,"saveModi: in");

        EditText nameBox = (EditText)findViewById(R.id.input_name);
        EditText telBox = (EditText)findViewById(R.id.input_tel);
        TextView dateBox = (TextView) findViewById(R.id.input_date);
        EditText locateBox = (EditText)findViewById(R.id.input_location);
        ImageView imageView = (ImageView)findViewById(R.id.profile_picture);

        String name = nameBox.getText().toString();
        String tel = telBox.getText().toString();
        String date = dateBox.getText().toString();
        String loc = locateBox.getText().toString();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ((BitmapDrawable)imageView.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.PNG,0,outputStream);

        byte[] pic = outputStream.toByteArray();

        Log.d(TAG,"saveModi: bytearray["+pic.length+"]");


        //init
        //no init

        DBHelper dbHelper =  AppManager.getInstance().getDbHelper();

        //end of method, go view table
        if(CURRENT_VIEW==REGI){
            dbHelper.insert(name,tel, date, loc, pic);
            goContentView(MENU);
        }else{
            dbHelper.update(name, tel, date, loc, pic);
            alignByname(null);
        }
    }

    /**
     * take picture in detail layout
     */
    void takePicture(View view){
        Log.d(TAG,"reached takePicture method");

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//dd        cameraIntent.
//        Bitmap bitmap;
//        cameraIntent.putExtra("taken",bitmap);
//https://developer.android.com/training/camera/photobasics.html 에서 참조.
        if(cameraIntent.resolveActivity(getPackageManager())!=null){
            startActivityForResult(cameraIntent,REQUEST_IMG_CAPTURE);
        }

    }

    void addRow(TableLayout targetTable, TableRow toAdding){
// reference the table layout
//        TableLayout tbl = (TableLayout)findViewById(R.id.RHE);
// delcare a new row
//        TableRow newRow = new TableRow(this);
// add views to the row
//        newRow.addView(new TextView(this)); // you would actually want to set properties on this before adding it
// add the row to the table layout
//        tbl.addView(newRow);
        targetTable.addView(toAdding);
    }
    void addRow(TableLayout targetTable, String name, String tel, String date, View.OnClickListener detail){
        TableRow newRow = new TableRow(this);
        View vName = new TextView(this);((TextView)vName).setText(name);
        View vTel = new TextView(this);((TextView)vTel).setText(tel);
        View vDate = new TextView(this);((TextView)vDate).setText(date);
        newRow.setOnClickListener(detail);

        newRow.addView(vName);
        newRow.addView(vTel);
        newRow.addView(vDate);
        newRow.addView(vName);

        addRow(targetTable,newRow);
    }

    public void insert(View target) {
        EditText textBox = (EditText)findViewById(R.id.input_name);
        textBox.getText().toString();
/**
        String name = (EditText)findViewById(R.id.input_name).text;
        String tel = edit_tel.getText().toString();
        db.execSQL("INSERT INTO contacts VALUES (null, '" + name + "', '" + tel
                + "');");
        Toast.makeText(getApplicationContext(), "성공적으로 추가되었음",
                Toast.LENGTH_SHORT).show();
        edit_name.setText("");
        edit_tel.setText("");
 */
    }

    public void search(View target) {
        /**
        String name = edit_name.getText().toString();
        Cursor cursor;
        cursor = db.rawQuery("SELECT name, tel FROM contacts WHERE name='"
                + name + "';", null);

        while (cursor.moveToNext()) {
            String tel = cursor.getString(1);
            edit_tel.setText(tel);
        }
         */
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMG_CAPTURE && resultCode == RESULT_OK) {
            ImageView pic = (ImageView)findViewById(R.id.profile_picture) ;

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            pic.setImageBitmap(imageBitmap);
        }
    }

}

