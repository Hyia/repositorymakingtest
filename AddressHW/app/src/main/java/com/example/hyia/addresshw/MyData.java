package com.example.hyia.addresshw;

import android.view.View;

/**
 * Created by hyia on 11/25/16.
 */

public class MyData {

    private String name;
    private String tel;
    private String date;
    private View.OnClickListener onClick;

    public MyData(){}
    public MyData(String name, String tel, String date, View.OnClickListener onClickListener){
        this.name = name;
        this.tel = tel;
        this.date = date;
        this.onClick = onClickListener;
    }

    public void setOnClick(View.OnClickListener onClick) {
        this.onClick = onClick;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public View.OnClickListener getOnClick() {
        return onClick;
    }

}
